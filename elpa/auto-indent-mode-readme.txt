
* About auto-indent-mode
Provides auto-indentation minor mode for Emacs.  This allows the
following:

  - Return automatically indents the code appropriately (if enabled)

  - Pasting/Yanking indents the appropriately

  - Killing line will take off unneeded spaces (if enabled)

  - On visit file, indent appropriately, but DONT SAVE. (Pretend like
    nothing happened, if enabled)

  - On save, optionally unttabify, remove trailing white-spaces, and
    definitely indent the file (if enabled).

  - TextMate behavior of keys if desired (see below)

  - Deleting the end of a line will shrink the whitespace to just one
    (if desired and enabled)

  - Automatically indent balanced parenthetical expression, or sexp, if desired
    `auto-indent-current-pairs' or `auto-indent-next-pair' is set
    to be true (disabled by default).  This is not immediate but occurs
    after a bit to allow better responsiveness in emacs.

  - Attempts to set the indentation level (number of spaces for an
    indent) for a major-mode.

All of these options can be customized. (customize auto-indent)
* Installing auto-indent-mode

To use put this in your load path and then put the following in your emacs
file:

  (setq auto-indent-on-visit-file t) ;; If you want auto-indent on for files
  (require 'auto-indent-mode)


If you (almost) always want this on, add the following to ~/.emacs:


   (auto-indent-global-mode)



Excluded modes are defined in `auto-indent-disabled-modes-list'

If you only want this on for a single mode, you would add the following to
~/.emacs


  (add-hook 'emacs-lisp-mode-hook 'auto-indent-minor-mode)



You could always turn on the minor mode with the command
`auto-indent-minor-mode'
* Setting the number of spaces for indenting major modes
While this is controlled by the major mode, as a convenience,
auto-indent-mode attempts to set the default number of spaces for an
indentation for specific major mode.

This is done by:
1. Making local variables of all the variables specified in
   `auto-indent-known-indent-level-variables' and setting them to
   auto-indent's `auto-indent-assign-indent-level'
2. Looking to see if major mode variables
   `major-mode-indent-level' and `major-mode-basic-offset' variables
   are present.  If either of these variables are present,
   `auto-indent-mode' sets these variables to the default
   `auto-indent-assign-indent-level'.

* TextMate Meta-Return behavior
If you would like TextMate behavior of Meta-RETURN going to the
end of the line and then inserting a newline, as well as
Meta-shift return going to the end of the line, inserting a
semi-colon then inserting a newline, use the following:


  (setq auto-indent-key-for-end-of-line-then-newline "<M-return>")
  (setq auto-indent-key-for-end-of-line-insert-char-then-newline "<M-S-return>")
  (require 'auto-indent-mode)
  (auto-indent-global-mode)


This may or may not work on your system.  Many times emacs cannot
distinguish between M-RET and M-S-RET, so if you don't mind a
slight redefinition use:


  (setq auto-indent-key-for-end-of-line-then-newline "<M-return>")
  (setq auto-indent-key-for-end-of-line-insert-char-then-newline "<C-M-return>")
  (require 'auto-indent-mode)
  (auto-indent-global-mode)


If you want to insert something other than a semi-colon (like a
colon) in a specific mode, say colon-mode, do the following:


  (add-hook 'colon-mode-hook (lambda () (setq auto-indent-eol-char ":")))

* Notes about autopair-mode and yasnippet compatibility
If you wish to use this with autopairs and yasnippet, please load
this library first.
* Using specific functions from auto-indent-mode

Also if you wish to just use specific functions from this library
that is possible as well.

To have the auto-indentation-paste use:


  (autoload 'auto-indent-yank "auto-indent-mode" "" t)
  (autoload 'auto-indent-yank-pop "auto-indent-mode" "" t)

  (define-key global-map [remap yank] 'auto-indent-yank)
  (define-key global-map [remap yank-pop] 'auto-indent-yank-pop)

  (autoload 'auto-indent-delete-char "auto-indent-mode" "" t)
  (define-key global-map [remap delete-char] 'auto-indent-delete-char)

  (autoload 'auto-indent-kill-line "auto-indent-mode" "" t)
  (define-key global-map [remap kill-line] 'auto-indent-kill-line)




However, this does not honor the excluded modes in
`auto-indent-disabled-modes-list'


* Making certain modes perform tasks on paste/yank.
Sometimes, like in R, it is convenient to paste c:\ and change it to
c:/.  This can be accomplished by modifying the
`auto-indent-after-yank-hook'.

The code for changing the paths is as follows:


  (defun kicker-ess-fix-path (beg end)
      "Fixes ess path"
      (save-restriction
        (save-excursion
          (narrow-to-region beg end)
          (goto-char (point-min))
          (when (looking-at "[A-Z]:\\\\")
            (while (search-forward "\\" nil t)
              (replace-match "/"))))))

    (defun kicker-ess-turn-on-fix-path ()
      (interactive)
      (when (string= "S" ess-language)
        (add-hook 'auto-indent-after-yank-hook 'kicker-ess-fix-path t t)))
    (add-hook 'ess-mode-hook 'kicker-ess-turn-on-fix-path)


Another R-hack is to take of the ">" and "+" of a command line
copy. For example copying:

 >
 > availDists <- c(Normal="rnorm", Exponential="rexp")
 > availKernels <- c("gaussian", "epanechnikov", "rectangular",
 + "triangular", "biweight", "cosine", "optcosine")


Should give the following code on paste:


 availDists <- c(Normal="rnorm", Exponential="rexp")
 availKernels <- c("gaussian", "epanechnikov", "rectangular",
 "triangular", "biweight", "cosine", "optcosine")


This is setup by the following code snippet:


  (defun kicker-ess-fix-code (beg end)
    "Fixes ess path"
    (save-restriction
      (save-excursion
        (save-match-data
          (narrow-to-region beg end)
          (goto-char (point-min))
          (while (re-search-forward "^[ \t]*[>][ \t]+" nil t)
            (replace-match "")
            (goto-char (point-at-eol))
            (while (looking-at "[ \t\n]*[+][ \t]+")
              (replace-match "\n")
              (goto-char (point-at-eol))))))))

  (defun kicker-ess-turn-on-fix-code ()
    (interactive)
    (when (string= "S" ess-language)
      (add-hook 'auto-indent-after-yank-hook 'kicker-ess-fix-code t t)))
  (add-hook 'ess-mode-hook 'kicker-ess-turn-on-fix-code)



* Auto-indent and org-mode
Auto-indent does not technically turn on for org-mode.  Instead the
following can be added/changed:

1. `org-indent-mode' is turned on when `auto-indent-start-org-indent'
   is true.
2. The return behavior is changed to newline and indent in code blocks
   when `auto-indent-fix-org-return' is true.
3. The backspace behavior is changed to auto-indent's backspace when
   `auto-indent-delete-backward-char' is true.  This only works in
   code blocks.
4. The home beginning of line behavior is changed to auto-indent's
   when `auto-indent-fix-org-move-beginning-of-line' is true.
5. The yank/paste behavior is changed to auto-indent in a code block
   when `auto-indent-fix-org-yank' is true.
6. The auto-filling activity in source-code blocks can break your code
   depending on the language.  When `auto-indent-fix-org-auto-fill' is
   true, auto-filling is turned off in`org-mode' source blocks.
* FAQ
** Why isn't my mode indenting?
Some modes are excluded for compatability reasons, such as
text-modes.  This is controlled by the variable
`auto-indent-disabled-modes-list'
** Why isn't my specific mode have the right number of spaces?
Actually, the number of spaces for indentation is controlled by the
major mode. If there is a major-mode specific variable that controls
this offset, you can add this variable to
`auto-indent-known-indent-level-variables' to change the indentation
for this mode when auto-indent-mode starts.

See:

- [[http://www.pement.org/emacs_tabs.htm][Understanding GNU Emacs and tabs]]
- [[http://kb.iu.edu/data/abde.html][In Emacs how can I change tab sizes?]]
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-move-beginning-of-line
Fixes `move-beginning-of-line' in `org-mode' when in source blocks to follow `auto-indent-mode'.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-interval
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
typing with `auto-indent-next-pair-timer-interval-addition'.  The
maximum that a particular mode can delay the timer is given by
`auto-indent-next-pair-timer-interval-max'.

*** auto-indent-next-pair-timer-interval-addition
If the indent operation for a file takes longer than the specified idle timer, grow that timer by this number for a particular mode.

*** auto-indent-next-pair-timer-interval-max
Maximum number seconds that auto-indent-mode will grow a parenthetical statement.
If this is less than or equal to zero, these will be no limit.

*** auto-indent-next-pairt-timer-interval-do-not-grow
If true, do not magically grow the mode-based indent time for a region.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-interval
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
typing with `auto-indent-next-pair-timer-interval-addition'.  The
maximum that a particular mode can delay the timer is given by
`auto-indent-next-pair-timer-interval-max'.

*** auto-indent-next-pair-timer-interval-addition
If the indent operation for a file takes longer than the specified idle timer, grow that timer by this number for a particular mode.

*** auto-indent-next-pair-timer-interval-max
Maximum number seconds that auto-indent-mode will grow a parenthetical statement.
If this is less than or equal to zero, these will be no limit.

*** auto-indent-next-pairt-timer-interval-do-not-grow
If true, do not magically grow the mode-based indent time for a region.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-move-beginning-of-line
Fixes `move-beginning-of-line' in `org-mode' when in source blocks to follow `auto-indent-mode'.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-geo-mean
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
to the geometric mean of rate to indent a single line.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-move-beginning-of-line
Fixes `move-beginning-of-line' in `org-mode' when in source blocks to follow `auto-indent-mode'.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-geo-mean
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
to the geometric mean of rate to indent a single line.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-move-beginning-of-line
Fixes `move-beginning-of-line' in `org-mode' when in source blocks to follow `auto-indent-mode'.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-geo-mean
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
to the geometric mean of rate to indent a single line.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
*Auto indentation on moving cursor to blank lines.

*** auto-indent-current-pairs
 - Automatically indent the current parenthetical statement.

*** auto-indent-delete-line-char-add-extra-spaces
 - When deleting a return, add a space (when appropriate)
between the newly joined lines.

This takes care of the condition when deleting text

Lorem ipsum dolor sit|
amet, consectetur adipiscing elit.  Morbi id

Lorem ipsum dolor sit|amet, consectetur adipiscing elit.  Morbi id

Which ideally should be deleted to:

Lorem ipsum dolor sit| amet, consectetur adipiscing elit.  Morbi id

This is controlled by the regular expressions in
`auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs'
and
`auto-indent-delete-line-char-add-extra-spaces-text-mode-regs'

*** auto-indent-delete-line-char-add-extra-spaces-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-add-extra-spaces-text-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-add-extra-spaces'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-extra-spaces
When deleting a return, delete any extra spaces between the newly joined lines.

*** auto-indent-delete-line-char-remove-last-space
Remove last space when deleting a line.

When `auto-indent-delete-line-char-remove-extra-spaces' is enabled,
expressions like lists can be removed in a less than optimal
manner.  For example, assuming =`|=' is the cursor:

c("Vehicle QD TO",|
     "1 ug IVT","3 ug IVT",...

would be deleted to the following

c("Vehicle QD TO",| "1 ug IVT","3 ug IVT",...

In this case it would be preferable to delete to:

c("Vehicle QD TO",|"1 ug IVT","3 ug IVT",...

However cases like sentences:

Lorem ipsum dolor sit amet,|
     consectetur adipiscing elit. Morbi id

Deletes to
Lorem ipsum dolor sit amet,| consectetur adipiscing elit. Morbi id

Which is a desired behavior.

When this is enabled, auto-indent attempts to be smarter by
deleting the extra space when characters before and after match
expressions defined in
`auto-indent-delete-line-char-remove-last-space-prog-mode-regs' and
`auto-indent-delete-line-char-remove-last-space-text-mode-regs'.

*** auto-indent-delete-line-char-remove-last-space-prog-mode-regs
 - Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for programming modes as determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-line-char-remove-last-space-text-mode-regs
Regular expressions for use with `auto-indent-delete-line-char-remove-last-space'.  This is used for modes other than programming modes.  This is determined by `auto-indent-is-prog-mode-p'.

*** auto-indent-delete-trailing-whitespace-on-save-file
 - When saving file delete trailing whitespace.

*** auto-indent-delete-trailing-whitespace-on-visit-file
 - Automatically remove trailing whitespace when visiting  file.

*** auto-indent-disabled-indent-functions
List of disabled indent functions.

List of functions that auto-indent ignores the `indent-region' on
paste and automated indent by pressing return.  The default is
`indent-relative' and `indent-relative-maybe'.  If these are used the
indentation is may not specified for the current mode.

*** auto-indent-disabled-modes-list
List of modes disabled when global `auto-indent-mode' is on.

*** auto-indent-disabled-modes-on-save
 - List of modes where `indent-region' of the whole file is ignored.

*** auto-indent-engine
Type of engine to use.  The possibilities are:

default: Use hooks and advices to implement auto-indent-mode

keymap: Use key remappings to implement auto-indent-mode.  This may
work in some modes but may cause things such as `company-mode' or
`auto-complete-mode' to function improperly

*** auto-indent-eol-char
End of line/statement character, like C or matlab's semi-colon.

Character inserted when
`auto-indent-key-for-end-of-line-inser-char-then-newline' is
defined.  This is a buffer local variable, therefore if you have
a mode that instead of using a semi-colon for an end of
statement, you use a colon, this can be added to the mode as
follows:

     (add-hook 'strange-mode-hook (lambda() (setq auto-indent-eol-char ":")))

autoThis is similar to Textmate's behavior.  This is useful when used
in conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-fix-org-auto-fill
Fixes org-based
  auto-fill-function (i.e. `org-auto-fill-function') to only
  auto-fill for things outside of a source block.

*** auto-indent-fix-org-backspace
Fixes `org-backspace' to use `auto-indent-backward-delete-char-behavior' for `org-mode' buffers.

*** auto-indent-fix-org-move-beginning-of-line
Fixes `move-beginning-of-line' in `org-mode' when in source blocks to follow `auto-indent-mode'.

*** auto-indent-fix-org-return
Allows newline and indent behavior in source code blocks in org-mode.

*** auto-indent-fix-org-yank
Allows org-mode yanks to be indented in source code blocks of org-mode.

*** auto-indent-force-interactive-advices
Forces interactive advices.

This makes sure that this is called when this is an interactive
call directly to the function.  However, if someone defines
something such as `org-delete-char' to delete a character, when
`org-delete-char' is called interactively and then calls
`delete-char' the advice is never activated (when it should be).
If this is activated, `auto-indent-mode' tries to do the right
thing by guessing what key should have been pressed to get this
event.  If it is the key that was pressed enable the advice.

*** auto-indent-home-is-beginning-of-indent
The Home key, or rather the `move-beginning-of-line' function, will move to the beginning of the indentation when called interactively.  If it is already at the beginning of the indent, move to the beginning of the line.

*** auto-indent-home-is-beginning-of-indent-when-spaces-follow
This is a customization for the home key.

If `auto-indent-home-is-beginning-of-indent' is enabled, the Home
key, or rather the `move-beginning-of-line' function, will move
to the beginning of the indentation when called interactively.

If it is already at the beginning of the indent,and move to the
beginning of the line.  When
`auto-indent-home-is-beginning-of-indent-when-spaces-follow' is
enabled, a home key press from

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
    | (let (at-beginning)

will change to

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
      |(let (at-beginning)

Another home-key will chang to cursor

    (defadvice move-beginning-of-line (around auto-indent-minor-mode-advice)
|   (let (at-beginning)

*** auto-indent-key-for-end-of-line-insert-char-then-newline
Key for end of line, `auto-indent-eol-char', then newline.

By default the `auto-indent-eol-char' is the semicolon. TextMate
uses shift-meta return, I believe (S-M-RET). If blank, no key is
defined.  The key should be in a format used for having keyboard
macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like
`autopair-mode'.

*** auto-indent-key-for-end-of-line-then-newline
Key for end of line, then newline.

TextMate uses meta return, I believe (M-RET).  If blank, no key
is defined. The key should be in a format used for saving
keyboard macros (see `edmacro-mode'). This is useful when used in
conjunction with something that pairs delimiters like `autopair-mode'.

*** auto-indent-kill-line-at-eol
Determines how a kill at the end of line behaves.

When killing lines, if at the end of a line,

nil - join next line to the current line.  Deletes white-space at
         join.  [this essentially duplicated delete-char]

         See also `auto-indent-kill-remove-extra-spaces'

whole-line - kill next lines

subsequent-whole-lines - merge lines on first call, subsequent kill whole lines

blanks - kill all empty lines after the current line, and then
            any lines specified.

You should also set the function `kill-whole-line' to do what you
want.

*** auto-indent-kill-line-kill-region-when-active
 - When killing lines, if region is active, kill region instead.

*** auto-indent-kill-remove-extra-spaces
 - Remove indentation before killing the line or region.

*** auto-indent-known-indent-level-variables
Known indent-level-variables for major modes.  Set locally when auto-indent-mode initializes.

*** auto-indent-known-text-modes
 - List of auto-indent's known text-modes.

*** auto-indent-minor-mode-symbol
 - When true, Auto Indent puts AI on the mode line.

*** auto-indent-mode-untabify-on-yank-or-paste
 - Untabify pasted or yanked region.

*** auto-indent-newline-function
 - Auto indentation function for the return key.

*** auto-indent-next-pair
Automatically indent the next parenthetical statement.  For example in R:

d| <- read.csv("dat.csv",
                  na.strings=c(".","NA"))

When typing .old, the indentation will be updated as follows:

d.old <- read.csv("dat.csv",
                     na.strings=c(".","NA"))

This will slow down your computation, so if you use it make sure
that the `auto-indent-next-pair-timer-interval' is appropriate
for your needs.

It is useful when using this option to have some sort of autopairing on.

*** auto-indent-next-pair-timer-geo-mean
Number of seconds before the observed parenthetical statement is indented.
The faster the value, the slower Emacs responsiveness but the
faster Emacs indents the region.  The slower the value, the
faster Emacs responds.  This should be changed dynamically by
to the geometric mean of rate to indent a single line.

*** auto-indent-on-save-file
 - Auto Indent on visit file.

*** auto-indent-on-visit-file
 - Auto Indent file upon visit.

*** auto-indent-on-visit-pretend-nothing-changed
 - When modifying the file on visit, pretend nothing changed.

*** auto-indent-on-yank-or-paste
 - Indent pasted or yanked region.

*** auto-indent-start-org-indent
Starts `org-indent-mode' when in org-mode.

*** auto-indent-untabify-on-save-file
 - Change tabs to spaces on file-save.

*** auto-indent-untabify-on-visit-file
 - Automatically convert tabs into spaces when visiting a file.

*** auto-indent-use-text-boundaries
Use text boundaries when killing lines.

When killing lines, if point is before any text, act as if
point is at BOL.  And if point is after text, act as if point
     is at EOL

** Internal Variables

*** auto-indent-eol-ret-save
Saved variable for keyboard state.

*** auto-indent-eol-ret-semi-save
Saved variable for keyboard state.

*** auto-indent-minor-mode-map
 - Auto Indent mode map.

*** auto-indent-pairs-begin
Defines where the pair region begins.

*** auto-indent-pairs-end
Defines where the pair region ends.
