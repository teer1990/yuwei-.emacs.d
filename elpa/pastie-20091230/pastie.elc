;ELC   
;;; Compiled by joe@nlp0.cs.nthu.edu.tw on Tue Nov 20 16:41:02 2012
;;; from file /home/joe/.emacs.d/elpa/pastie-20091230/pastie.el
;;; in Emacs version 24.2.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(custom-declare-group 'pastie nil "Interface to pastie.org" :tag "Pastie" :group 'applications)
#@39 When non-nil, creates private pastes.
(custom-declare-variable '*pastie-restricted* 't '(#$ . 625) :type 'boolean :group 'pastie)
#@53 The time, in seconds, to wait for a pastie request.
(custom-declare-variable '*pastie-timeout* '10 '(#$ . 761) :type 'integer :group 'pastie)
#@22 The last url pasted.
(defvar *pastie-last-url* "" (#$ . 909))
#@41 The buffer used to show fetched pastes.
(defvar *pastie-buffer* nil (#$ . 977))
#@60 Sniffs for the language of the region that is being pasted
(defalias 'pastie-language #[nil "\303\300!\203\f \203\f \304\207\303\301!\203 	\203 \305\207\306\n\307\"A\206! \310\207" [rails-view-minor-mode rails-minor-mode major-mode boundp "html_rails" "ruby_on_rails" assoc ((c-mode . "c++") (c++-mode . "c++") (css-mode . "css") (diff-mode . "diff") (html-mode . "html") (java-mode . "java") (python-mode . "python") (javascript-mode . "javascript") (js2-mode . "javascript") (jde-mode . "java") (php-mode . "php") (ruby-mode . "ruby") (text-mode . "plain_text") (sql-mode . "sql") (sh-mode . "shell-unix-generic")) "plain_text"] 3 (#$ . 1063)])
(defalias 'pastie-url-format #[nil "\203 \301\207\302\207" [*pastie-restricted* "http://pastie.org/private/%s" "http://pastie.org/paste/%s"] 1])
#@166 Post the current region as a new paste at pastie.org.
Copies the URL into the kill ring.

With a prefix argument, toggle the current value of
`*pastie-restricted*'.
(defalias 'pastie-region #[(begin end &optional toggle-restricted) "\306	\"\307\310\311\n#\312 \313\314\315\316\317!\203&  ?\202(   \320\321\f\322\323 \2055 \324\325\326\327\260\n!\330\331\215\211\"\330=\203N \332\333!\202P \".\n\207" [begin end body-raw body mode url-request-method buffer-substring-no-properties replace-regexp-in-string "[<>&]" #[(match) "\301!\302=\203\n \303\207\301!\304=\203 \305\207\301!\306=\205 \307\207" [match string-to-char 60 "<" 62 ">" 38 "&"] 2] pastie-language "POST" "application/xml" (("Content-Type" . "application/xml")) url-generic-parse-url "http://pastie.org/pastes" "<paste>" "<parser>" "</parser>" "<authorization>burger</authorization>" "<restricted>1</restricted>" "<body>" "</body>" "</paste>" #1=#:timeout (byte-code "\304\305\306#\211\nB\307\216\310!+\207" [*pastie-timeout* -with-timeout-timer- with-timeout-timers url run-with-timer nil #[nil "\300\301\211\"\207" [throw #1#] 3] ((cancel-timer -with-timeout-timer-)) pastie-retrieve] 5) error "Pastie timed out." url-mime-accept-string url-request-extra-headers url toggle-restricted *pastie-restricted* url-request-data -with-timeout-value-] 11 (#$ . 1867) "r\nP"])
#@49 Submits the request and validates the response.
(defalias 'pastie-retrieve #[(url) "\305!r	q\210eb\210\306\307!\210\310\311!\312\313\n\"\2039 db\210\314 \210`d{\315\316 \"\317\320\"\210\321!\210*\202> \317\322\n\"\210*\323	!)\207" [url *pastie-buffer* status id *pastie-last-url* url-retrieve-synchronously search-forward-regexp "^Status: \\([0-9]+.*\\)" match-string 1 string-match "^20[01]" beginning-of-line format pastie-url-format message "Paste created: %s" kill-new "Error occured: %s" kill-buffer] 3 (#$ . 3235)])
#@166 Post the current buffer as a new paste at pastie.org.
Copies the URL into the kill ring.

With a prefix argument, toggle the current value of
`*pastie-restricted*'.
(defalias 'pastie-buffer #[(&optional toggle-restricted) "\301ed#\207" [toggle-restricted pastie-region] 4 (#$ . 3774) "P"])
#@68 Fetch the contents of the paste from pastie.org into a new buffer.
(defalias 'pastie-get #[(id) "\306\307\310\311\312\313\"!\314	!rq\210eb\210\315\316!\210\317\320!\321\322\"\203H \315\323!\210\324\317\320!!\210\315\325!\210e`|\210\326 \210\327\307!\210\330!\202Q \331\332\"\210\333!.\207" [id url url-mime-accept-string url-request-extra-headers url-request-method *pastie-buffer* "GET" nil "*/*" url-generic-parse-url format "http://pastie.org/pastes/%s/download" url-retrieve-synchronously search-forward-regexp "^Status: \\([0-9]+.*\\)" match-string 1 string-match "^200" "^Content-Disposition: attachment; filename=\"\\(.*\\)\"" set-visited-file-name "\n\n" normal-mode set-buffer-modified-p switch-to-buffer message "Error occured: %s" kill-buffer status] 7 (#$ . 4071) "nPastie #: "])
(defalias 'pastie-browse #[nil "\301!\207" [*pastie-last-url* browse-url] 2 nil nil])
(provide 'pastie)
