;;;;;;;;;;;;;;;; load local elisps ;;;;;;;;;;;;;;;;
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/")
(add-to-list 'load-path "~/.emacs.d/local-lisp/")

;;;;;;;;;;;;;;;;;;;; hooks ;;;;;;;;;;;;;;;;;;;;
(add-hook 'c-mode-hook
	  ( lambda ()
	    (define-key c-mode-map (kbd "C-c <C-return>") 'compile)))
(add-hook 'c++-mode-hook
	  ( lambda ()
	    (define-key c++-mode-map (kbd "C-c <C-return>") 'compile)))

;;;;;;;;;;;;;;;; bindings ;;;;;;;;;;;;;;;;
(define-key global-map (kbd "C-<tab>") 'dabbrev-expand)
(define-key global-map (kbd "C-\\") 'set-mark-command)
(define-key global-map (kbd "C-x C-\\") 'pop-global-mark)
(define-key global-map (kbd "C-<return>") 'view-mode)
(define-key global-map (kbd "s-b") 'ido-switch-buffer)
(define-key global-map (kbd "C-x C-b") 'buffer-menu)
(global-set-key [(meta x)] (lambda ()
                             (interactive)
                             (or (boundp 'smex-cache)
                                 (smex-initialize))
                             (global-set-key [(meta x)] 'smex)
                             (smex)))

(global-set-key [(shift meta x)] (lambda ()
                                   (interactive)
                                   (or (boundp 'smex-cache)
                                       (smex-initialize))
                                   (global-set-key [(shift meta x)] 'smex-major-mode-commands)
                                   (smex-major-mode-commands)))




;;;;;;;;;;;;;;;;;;;; Custom ;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu 0.5)
 '(ac-auto-start t)
 '(ac-use-fuzzy t)
 '(ac-use-menu-map t)
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(auto-save-default nil)
 '(autopair-global-mode t)
 '(backup-inhibited t t)
 '(bookmark-default-file "~/.emacs.d/bookmark")
 '(company-idle-delay 0.5)
 '(cua-enable-cua-keys nil)
 '(current-language-environment "UTF-8")
 '(cursor-type (quote bar) t)
 '(custom-safe-themes (quote ("501caa208affa1145ccbb4b74b6cd66c3091e41c5bb66c677feda9def5eab19c" "72cc9ae08503b8e977801c6d6ec17043b55313cda34bcf0e6921f2f04cf2da56" "24377c1fcac8c5bf60b8df90dd43690f85c03223a27c65f50ca3560c863d59ac" "71efabb175ea1cf5c9768f10dad62bb2606f41d110152f4ace675325d28df8bd" "bf7ed640479049f1d74319ed004a9821072c1d9331bc1147e01d22748c18ebdf" "78b1c94c1298bbe80ae7f49286e720be25665dca4b89aea16c60dacccfbb0bca" "3580fb8e37ee9e0bcb60762b81260290329a97f3ca19249569d404fce422342f" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default)))
 '(dired-find-subdir t)
 '(dired-guess-shell-gnutar "gtar")
 '(dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\.")
 '(echo-keystrokes 0.01)
 '(edit-server-verbose t)
 '(electric-indent-mode nil)
 '(electric-layout-mode t)
 '(electric-pair-mode nil)
 '(emacs-lisp-mode-hook (quote (#[nil "\300\301!\207" [checkdoc-minor-mode 1] 2] turn-on-eldoc-mode imenu-add-menubar-index)))
 '(exec-path (quote ("/usr/bin" "/bin" "/usr/sbin" "/sbin" "/Applications/Emacs.app/Contents/MacOS/bin" "/usr/local/bin" "/opt/my/bin" nil)))
 '(fci-rule-color "#383838")
 '(fill-column 78)
 '(frame-title-format (quote ("%f - " user-real-login-name "@" system-name)) t)
 '(global-auto-complete-mode t)
 '(global-company-mode nil)
 '(global-highlight-changes-mode nil)
 '(global-hl-line-mode t)
 '(global-linum-mode t)
 '(global-semantic-decoration-mode t)
 '(global-semantic-highlight-edits-mode t)
 '(global-semantic-highlight-func-mode t)
 '(global-semantic-mru-bookmark-mode t)
 '(global-semantic-show-unmatched-syntax-mode t)
 '(global-semantic-stickyfunc-mode t)
 '(helm-mode t)
 '(highlight-symbol-idle-delay 0)
 '(ido-auto-merge-work-directories-length nil)
 '(ido-create-new-buffer (quote always))
 '(ido-enable-flex-matching t)
 '(ido-enable-prefix nil)
 '(ido-everywhere t)
 '(ido-ignore-extensions t)
 '(ido-max-prospects 8)
 '(ido-mode (quote both) nil (ido))
 '(ido-use-filename-at-point (quote guess))
 '(indent-tabs-mode nil)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(keyboard-coding-system (quote utf-8-unix))
 '(linum-format "  %d  ")
 '(lisp-interaction-mode-hook (quote (turn-on-eldoc-mode)))
 '(lisp-mode-hook (quote (imenu-add-menubar-index slime-lisp-mode-hook)))
 '(mf-offset-x 54)
 '(mouse-wheel-scroll-amount (quote (1 ((shift) . 1) ((control)))))
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("marmalade" . "http://marmalade-repo.org/packages/"))))
 '(package-enable-at-startup t)
 '(package-load-list (quote (all auto-complete)))
 '(puppet-indent-level tab-width)
 '(py-pychecker-command "pyflakes")
 '(python-indent-guess-indent-offset t)
 '(python-indent-offset 4)
 '(python-skeleton-autoinsert t)
 '(recentf-max-saved-items 75)
 '(require-final-newline t)
 '(ruby-indent-level tab-width)
 '(scroll-preserve-screen-position t)
 '(semantic-default-submodes (quote (global-semantic-highlight-func-mode global-semantic-decoration-mode global-semantic-stickyfunc-mode global-semantic-idle-completions-mode global-semantic-idle-scheduler-mode global-semanticdb-minor-mode global-semantic-idle-summary-mode global-semantic-mru-bookmark-mode)))
 '(semantic-mode nil)
 '(server-host nil)
 '(server-mode t)
 '(server-use-tcp t)
 '(set-mark-command-repeat-pop t)
 '(show-paren-delay 0)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(virtualenv-root "~/.pythonbrew/venvs/Python-2.7.3"))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-symbol-face ((t (:background "alternateSelectedControlColor"))))
 '(highline-face ((t (:background "windowBackgroundColor"))) t)
 '(highline-vertical-face ((t (:background "windowBackgroundColor"))) t)
 '(show-paren-match ((t (:background "Purple")))))

;;;;;;;;;;;;;;;;;;;; package ;;;;;;;;;;;;;;;;;;;;
(require 'package)
(package-initialize)
;;;;;;;;;;;;;;;;;;;; fold-dwim ;;;;;;;;;;;;;;;;;;;;
(require 'fold-dwim)
;;;;;;;;;;;;;;;;;;;; auto-complete ;;;;;;;;;;;;;;;;;;;;
(require 'auto-complete)
(global-auto-complete-mode t)
;;;;;;;;;;;;;;;;;;;; highlight-symbol ;;;;;;;;;;;;;;;;;;;;
(require 'highlight-symbol)
(define-globalized-minor-mode global-highlight-symbol-mode highlight-symbol-mode (lambda ()(highlight-symbol-mode t)))
(global-highlight-symbol-mode t)
;;;;;;;;;;;;;;;;;;;; autopair ;;;;;;;;;;;;;;;;
(autopair-global-mode t)
;;;;;;;;;;;;;;;; helm ;;;;;;;;;;;;;;;;
(helm-mode t)

;;;;;;;;;;;;;;;;;;;; comment current line ;;;;;;;;;;;;;;;;;;;;
(require 'comment-dwim-line)
(global-set-key "\M-;" 'comment-dwim-line)

;;;;;;;;;;;;;;;;;;;; ropemacs ;;;;;;;;;;;;;;;;;;;;
;; (require 'python)
;; (autoload 'pymacs-apply "pymacs")
;; (autoload 'pymacs-call "pymacs")
;; (autoload 'pymacs-eval "pymacs" nil t)
;; (autoload 'pymacs-exec "pymacs" nil t)
;; (autoload 'pymacs-load "pymacs" nil t)
;; (pymacs-load "ropemacs" "rope-")
;; (setq ropemacs-enable-autoimport t)

;;;;;;;;;;;;;;;;;;;; Tramp ;;;;;;;;;;;;;;;;;;;;
(require 'tramp)
;(add-to-list 'tramp-default-proxies-alist '("nlp-s1" nil "/ssh:nlp-service1.cs.nthu.edu.tw#2222:"))

(add-to-list 'tramp-default-proxies-alist
	     '(nil  "\\`root\\'" "/ssh:%h:"))
(add-to-list 'tramp-default-proxies-alist
	     '((regexp-quote (system-name)) nil nil))

(if (window-system)
    (load-theme 'monokai t)
  (load-theme 'wombat t))
